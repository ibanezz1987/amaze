import { Component, OnInit } from '@angular/core';
import { Vector3 } from '../classes/vector3';

@Component({
  selector: 'app-css',
  templateUrl: './css.component.html',
  styleUrls: ['./css.component.scss']
})
export class CssComponent implements OnInit {
    isEditorActive:boolean = false;
    player:Vector3 = new Vector3(1, 8, 2);
    cursor:Vector3 = new Vector3(0, 0, 0);
    selectedSurface:number = 1;
    surfaceTypes:number[] = [0, 1, 2, 3];
    waterLevel:number = 1;
    model:number[][][] = [[[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]],[[null,null,null,null,1,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,1,1,null,null,null,null,null,null,1,1,null],[null,null,null,null,null,null,null,2,null,null,null,null,1,1,1,null],[null,null,null,null,null,null,null,null,null,null,null,1,1,1,1,null],[null,null,null,null,null,null,null,null,null,null,1,1,1,1,1,null],[2,2,null,null,null,null,null,null,null,1,1,1,1,1,1,null],[1,1,2,2,null,null,null,null,1,1,1,1,null,1,1,null],[1,1,1,1,null,null,null,1,1,1,1,1,null,1,1,null],[1,1,1,2,null,null,null,null,1,1,1,1,1,1,1,null],[1,1,1,null,null,null,null,null,null,1,1,1,1,1,1,null],[null,null,null,null,null,null,null,null,null,null,1,1,1,1,1,null],[1,1,1,null,null,null,null,null,null,null,null,1,1,1,1,null],[1,1,1,2,null,null,null,null,null,null,null,null,1,1,1,null],[1,1,1,2,2,null,null,null,null,null,null,null,null,null,null,null]],[[null,null,null,null,1,1,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,1,1,1,null,null,null,null,null,1,1,null],[null,null,null,null,null,null,null,null,null,null,null,null,1,1,1,null],[null,null,null,null,null,null,null,null,null,null,null,1,1,1,1,null],[null,null,null,null,null,null,null,null,null,null,1,1,1,1,1,null],[null,null,null,null,null,null,null,null,null,1,1,1,1,1,1,null],[1,1,null,null,null,null,null,null,1,1,1,1,null,1,1,null],[1,1,1,1,3,3,3,1,1,1,1,1,null,1,1,null],[1,1,1,null,null,null,null,null,1,1,1,1,1,1,1,null],[1,1,null,null,null,null,null,null,null,1,1,1,1,1,1,null],[3,null,null,null,null,null,null,null,null,null,1,1,1,1,1,null],[1,1,1,null,null,null,null,null,null,null,null,1,1,1,1,null],[1,1,1,null,null,null,null,null,null,null,null,null,1,1,1,null],[1,1,null,null,null,null,null,null,null,null,null,null,null,null,null,null]],[[null,null,null,null,null,null,1,1,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,1,1,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,1,null,null],[null,null,null,null,null,null,null,null,null,null,null,1,null,null,1,null],[null,null,null,null,null,null,null,null,null,null,1,null,null,null,null,null],[1,1,null,null,null,null,null,null,null,1,null,null,null,null,1,null],[null,null,null,null,null,null,null,null,null,null,1,null,null,null,1,null],[null,null,null,null,null,null,null,null,null,null,null,null,1,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,1,null,null,1,null],[null,null,null,null,null,null,null,null,null,null,null,1,null,1,null,null],[1,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null]]];
    cellSize:number = 70;
    layerHeight:number = 35;

    constructor() { }

    ngOnInit(): void {
        window.addEventListener("keydown", event => {
            this.handleKeyPress(event.key);
        });
    }

    handleKeyPress(keyCode:string):void {
        if(this.isEditorActive) {
            if(keyCode === 'ArrowUp' || keyCode === 'w') {
                this.moveCursor(0, -1);
            }
            else if(keyCode === 'ArrowLeft' || keyCode === 'a') {
                this.moveCursor(-1, 0);
            }
            else if(keyCode === 'ArrowDown' || keyCode === 's') {
                this.moveCursor(0, 1);
            }
            else if(keyCode === 'ArrowRight' || keyCode === 'd') {
                this.moveCursor(1, 0);
            }
            else if(keyCode === 'q') {
                this.decreaseElevation();
            }
            else if(keyCode === 'e') {
                this.increaseElevation();
            }
            else if(keyCode === 'x') {
                this.isEditorActive = false;
            }
            else if(keyCode === 'p') {
                console.log(JSON.stringify(this.model));
            }
            else if(this.surfaceTypes.indexOf(Number(keyCode)) !== -1) {
                // Note: casting to number could potentially cause a bug
                this.selectedSurface = Number(keyCode);

                this.changeTerrain();
            }
        }
        else {
            if(keyCode === 'ArrowUp' || keyCode === 'w') {
                this.movePlayer(0, -1);
            }
            else if(keyCode === 'ArrowLeft' || keyCode === 'a') {
                this.movePlayer(-1, 0);
            }
            else if(keyCode === 'ArrowDown' || keyCode === 's') {
                this.movePlayer(0, 1);
            }
            else if(keyCode === 'ArrowRight' || keyCode === 'd') {
                this.movePlayer(1, 0);
            }
            else if(keyCode === 'x') {
                this.isEditorActive = true;
            }
        }
    }

    movePlayer(xChange:number, yChange:number):void {
        const newX:number = this.player.x + xChange;
        const newY:number = this.player.y + yChange;

        if(this.isWithinBoundaries(newX, newY)) {
            const newZ:number = this.getZValueAt(newX, newY);

            if(newZ === this.player.z) {
                this.player.x = newX;
                this.player.y = newY;
                this.player.z = this.getZValueAt(this.player.x, this.player.y);
            }
        }
    }

    moveCursor(xChange:number, yChange:number):void {
        const newX:number = this.cursor.x + xChange;
        const newY:number = this.cursor.y + yChange;

        if(this.isWithinBoundaries(newX, newY)) {
            this.cursor.x = newX;
            this.cursor.y = newY;
            this.cursor.z = this.getZValueAt(this.cursor.x, this.cursor.y);
        }
    }

    isWithinBoundaries(x:number, y:number):boolean {
        if(!this.model.length || !this.model[0].length) {
            return false;
        }

        return x >= 0 && x < this.model[0][0].length && y >= 0 && y < this.model[0].length;
    }

    getZValueAt(x:number, y:number):number {
        for(let i = this.model.length - 1; i >= 0; i--) {
            const layer:number[][] = this.model[i];
            const cell:number = layer[y][x];

            if(cell !== null && cell !== 0) {
                return i;
            }
        }

        return 0;
    }

    increaseElevation():void {
        if(this.cursor.z < this.model.length - 1) {
            this.cursor.z++;
            this.model[this.cursor.z][this.cursor.y][this.cursor.x] = this.selectedSurface;
        }
    }

    decreaseElevation():void {
        if(this.cursor.z > 0) {
            this.model[this.cursor.z][this.cursor.y][this.cursor.x] = null;
            this.cursor.z--;
        }
    }

    changeTerrain():void {
        this.model[this.cursor.z][this.cursor.y][this.cursor.x] = this.selectedSurface;
    }

    isPlayerHere(layerIndex:number, rowIndex:number, cellIndex:number):boolean {
        return this.player.x === cellIndex && this.player.y === rowIndex && this.player.z === layerIndex;
    }

    isCursorHere(layerIndex:number, rowIndex:number, cellIndex:number):boolean {
        return this.cursor.x === cellIndex && this.cursor.y === rowIndex && this.cursor.z === layerIndex;
    }

    hasLeftPane(row:number[], cell:number, cellIndex:number):boolean {
        const neighborCell:number = row[cellIndex - 1];

        if(neighborCell === cell) {
            return false;
        }

        return true;
    }

    hasRightPane(layer:number[][], cell:number, rowIndex:number, cellIndex:number):boolean {
        const neighborRow:number[] = layer[rowIndex + 1];

        if(!neighborRow) {
            return true;
        }
        else if(cell === null) {
            return false;
        }

        const neighborCell:number = neighborRow[cellIndex];

        if(neighborCell !== null && neighborCell === cell) {
            return false;
        }
        
        return true;
    }
}