import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { ObservablesComponent } from './observables/observables.component';
import { UnittestComponent } from './unittest/unittest.component';
import { MyCounterComponent } from './my-counter/my-counter.component';
import { CssComponent } from './css/css.component';


const routes: Routes = [
  { path: '', component: CssComponent },
  { path: 'css', component: CssComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'observables', component: ObservablesComponent },
  { path: 'unittest', component: UnittestComponent },
  { path: 'ngrx', component: MyCounterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
