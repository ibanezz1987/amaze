export class Vector2 {
    x:number = 0;
    y:number = 0;

    constructor(x?:number, y?:number) {
        this.x = typeof x !== 'undefined' ? x : 0;
        this.y = typeof y !== 'undefined' ? y : 0;
    }

    multiplyWith(v):any {
        if (v.constructor === Vector2) {
            this.x *= v.x;
            this.y *= v.y;
        }
        else if (v.constructor === Number) {
            this.x *= v;
            this.y *= v;
        }
        return this;
    }

    addTo(v):any {
        if (v.constructor === Vector2) {
            this.x += v.x;
            this.y += v.y;
        }
        else if (v.constructor === Number) {
            this.x += v;
            this.y += v;
        }
        return this;
    }

    add(v):any {
        var result = this.copy();
        return result.addTo(v);
    }

    multiply(v):any {
        var result = this.copy();
        return result.multiplyWith(v);
    }

    copy():any {
        return new Vector2(this.x, this.y);
    }
}

