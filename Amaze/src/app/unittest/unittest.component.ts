import { Component, OnInit } from '@angular/core';

@Component({
selector: 'app-unittest',
templateUrl: './unittest.component.html',
styleUrls: ['./unittest.component.scss']
})
export class UnittestComponent {
    myArray = [11, 11, 12, 13, 14, 15];

    constructor() { }

    addNumbers(first, second) {
        return first + second;
    }

    addItem(item) {
        this.myArray.push(item);
    }

    removeValue(value) {
        for (let i = this.myArray.length; i >= 0; i--) {
            const item = this.myArray[i];
            
            if(item === value) {
                this.myArray.splice(i, 1);
            }
        }
    }

}
