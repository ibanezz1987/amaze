import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnittestComponent } from './unittest.component';

describe('UnittestComponent', () => {
    let component: UnittestComponent;
  
    beforeEach(() => {
      component = new UnittestComponent();
    })

    describe('myArray', () => {
        it('should be [11, 11, 12, 13, 14, 15]', () => {
            expect(component.myArray).toEqual([11, 11, 12, 13, 14, 15]);
        });
    });

    describe('addItem', () => {
        it('myArray should be [11, 11, 12, 13, 14, 15, 16]', () => {
            component.addItem(16);
            expect(component.myArray).toEqual([11, 11, 12, 13, 14, 15, 16]);
        });
    });

    describe('removeValue', () => {
        it('myArray should be [12, 13, 14, 15]', () => {
            component.removeValue(11);

            expect(component.myArray).toEqual([12, 13, 14, 15]);
        });
    });

    describe('addNumbers', () => {
        it('2 + 2 should be 4', () => {
            expect(component.addNumbers(2, 2)).toBe(4);
        });
    });
});
