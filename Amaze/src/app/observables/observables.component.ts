import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ConfigService } from '../services/config.service';

@Component({
    selector: 'app-observables',
    templateUrl: './observables.component.html',
    styleUrls: ['./observables.component.scss']
})
export class ObservablesComponent {
    config:any;

    constructor(private configService:ConfigService) {
        // Stop listening after 100 seconds
        setTimeout(() => {
            this.randomNumbersSubscription.unsubscribe();
        }, 100000);
    }

    showConfig() {
        this.configService.getConfig()
          .subscribe((data: any) => this.config = data);
    }

    randomNumbers = new Observable((observer) => {
        let randomNumber = setInterval(() => {
            const random = Math.random();

            if(random) {
                observer.next(random);
            }
            else {
                // Example of an error implementation (obviously not working here)
                observer.error('error');
            }
        }, 3000);

        // When the consumer unsubscribes, clean up data ready for next subscription.
        return {
            unsubscribe() {
                // // Example code:
                // navigator.geolocation.clearWatch(watchId);
            }
        };
    });

    randomNumbersSubscription = this.randomNumbers.subscribe({
        next(number) {
            console.log('Random number: ', number);
        },
        error(msg) {
            console.log('Error Getting Location: ', msg);
        }
    });
}
