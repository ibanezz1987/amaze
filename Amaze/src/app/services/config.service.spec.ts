import { ConfigService } from './config.service';

describe('ConfigService', () => {
  let service: ConfigService;
  let mockHttp: any;


  beforeEach(() => {
    mockHttp = jasmine.createSpyObj('mockHttp', ['get']);
    service = new ConfigService(mockHttp);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});