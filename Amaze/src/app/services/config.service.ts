import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IEvent } from './../shared/event.model';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
    configUrl = 'http://localhost:3000/artists/';

    constructor(private http: HttpClient) { }

    getConfig() {
        return this.http.get(this.configUrl)
            .pipe(catchError(this.handleError<IEvent[]>('getConfig()', [])));
    }

    private handleError<T> (operation = 'operation', result?:T) {
        return (error: any): Observable<T> => {
            console.log('handleError', error);
            return of(result as T);
        }
    }
}
